//
//  RunViewController.swift
//  EvoBot
//
//  Created by user on 30/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//

import UIKit

class RunViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBAction func load(sender: UIButton) {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.AllDomainsMask, true)
        let path: AnyObject = paths[0]
        let arrPath = path.stringByAppendingString("/to.plist")
        NSKeyedArchiver.archiveRootObject(allData!, toFile: arrPath)
            //allData!.writeToURL(url, atomically = true)
    
            let temp: [SelectedData]? = NSKeyedUnarchiver.unarchiveObjectWithFile(arrPath) as? [SelectedData]
            print (temp![0].selectedVol)
            
        
        
        
    }
    
    @IBAction func saveExperiment(sender: UIButton) {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.AllDomainsMask, true)
        let path: AnyObject = paths[0]
        //let arrPath = path.stringByAppendingString("/array.plist")
        //NSKeyedArchiver.archiveRootObject(allData!, toFile: arrPath)
        
        
        let alert = UIAlertController(title: "Save Experiment", message: "Enter the experiment name", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) -> Void in
            let tf = alert.textFields?.first
            if tf != nil{
            let arrPath = path.stringByAppendingString((tf!.text)!)
            NSKeyedArchiver.archiveRootObject(self.allData!, toFile: arrPath)
            print ("sth")
            }
        }))
        alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Enter File Name"
        }
        
        
        /*
        alert.addAction(/* cancel button action */)
       
        
        
        alert.addAction(UIAlertAction(
            title: “Login”,
            style: .Default)
            { (action: UIAlertAction) -> Void in
            // get password and log in
            let tf = self.alert.textFields?.first as? UITextField
            if tf != nil { self.loginWithPassword(tf.text) }
            } )
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = “Guidance System Password”
        }
*/
        presentViewController(alert, animated: true, completion: nil)
        
        
        
        
       
    }
    
    @IBOutlet weak var tableView: UITableView!
    var allData:[SelectedData]?{didSet{
        print ("jadid \(allData)")}}
    override func viewDidLoad() {
        let editBarButtonItem = UIBarButtonItem(title: "Edit", style: .Plain, target:self, action: "editAction:")
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.setRightBarButtonItem(editBarButtonItem, animated: true)
        //self.navigationItem.rightBarButtonItem = editBarButtonItem
        navigationItem.title = "Current Experiment"
        



    }
    func editAction(sender: UIBarButtonItem){
        if sender.title == "Edit"{
            
        sender.title = "Done"}
        else if sender.title == "Done"{
        sender.title = "Edit"}
        tableView.setEditing(!tableView.editing, animated: true)

    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let allTheData = allData{
            return allTheData.count
        }
        return 1
        
        
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Experiment Steps", forIndexPath: indexPath) as? RunExperimentTableViewCell
        if let allTheData = allData{
        cell!.volLabel.text = "\(allTheData[indexPath.row].selectedVol!)"
        cell!.unitLabel.text = "\(allTheData[indexPath.row].selectedVolUnit!)"
        cell!.syringeLabel.text = "\(allTheData[indexPath.row].selectedSyringe!)"
        cell!.startPointLabel.text = "\(allTheData[indexPath.row].startPoint!)"
        cell!.endPointLabel.text = "\(allTheData[indexPath.row].endPoint!)"
    
            
        }
        // Configure the cell...
        
        return cell!
    }
    
    
    
    // Override to support conditional editing of the table view.
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }

    
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    allData!.removeAtIndex(indexPath.row)

        
        
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        //let chiz = SelectedData()
       // allData!.append(chiz)

    }
    }
    
    
    
    // Override to support rearranging the table view.
    func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        
        let data = allData![fromIndexPath.row]
        allData!.removeAtIndex(fromIndexPath.row)
        allData!.insert(data, atIndex: toIndexPath.row)
    
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @IBAction func runExperiment(sender: UIButton) {
        
        //let urlString = String(format: "http://10.16.79.38:9000/?x=5&y=3")
        
        var urls = "?"
        for data in allData!{
            let url:String = "step:" + data.startPoint! + "&" + data.endPoint! + "&" + data.selectedSyringe! + "&" + "\(data.selectedVol!)"
            urls = urls + url
            
        }
        print (urls)

        
        //let urlString = String(format: "http://127.0.0.1:9000/?x=5&y=3")
        let urlString = "http://127.0.0.1:9000/" + urls
        print(urlString)
        let safeURL = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        let url = NSURL(string: safeURL)!
        let request = NSURLRequest(URL: url)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false;
            
            if let error = error {
                print(error)
                return
            }
            
            // Parse JSON data
            if let data = data {
                let dataString = NSString(data: data, encoding: NSUTF8StringEncoding) ?? ""
                print(dataString)
                
                
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in

                })
                
            }
            
        })
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true;
        task.resume()
    }

        
        
        
    
    

}
