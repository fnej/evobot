//
//  RunExperimentTableViewCell.swift
//  EvoBot
//
//  Created by user on 30/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//

import UIKit

class RunExperimentTableViewCell: UITableViewCell {

    @IBOutlet weak var startPointLabel: UILabel!
    @IBOutlet weak var endPointLabel: UILabel!
    @IBOutlet weak var volLabel: UILabel!
    
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var syringeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
