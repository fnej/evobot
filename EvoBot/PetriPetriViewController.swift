//
//  PetriPetriViewController.swift
//  EvoBot
//
//  Created by user on 28/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//


import UIKit

class PetriPetriViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var syringePickerView: UIPickerView!
    @IBOutlet weak var volsPickerView: UIPickerView!
    var vols = Array(1...20).map({ $0 * 5})
    let units = ["ul", "ml"]
    var selectedData : SelectedData?
    let volsUL = Array(1...20).map({ $0 * 5})
    let volsML = Array(1...100).map({ $0 * 1})
    let syringes = ["1", "2", "3"]
    
    
    
    @IBAction func dismissController(sender: UIButton) {
        presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func but(sender: AnyObject) {
        print ("1")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        volsPickerView.delegate = self
        volsPickerView.dataSource = self
        selectedData = SelectedData(selectedVol: vols[0], selectedVolUnit: units[0], selectedSyringe: syringes[0], startPoint:"", endPoint: "")
        
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print ("segued")
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        switch(pickerView){
        case volsPickerView:
            return 2
        case syringePickerView:
            return 1
        default:
            return 0
        }//end of switch
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch (pickerView){
        case volsPickerView:
            switch component{
            case 0:
                return vols.count
            case 1:
                return units.count
            default:
                return 0
            }//end of switch
        case syringePickerView:
            return syringes.count
        default:
            return 0
        }//end of switch
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView{
        case volsPickerView:
            switch component{
            case 0:
                return "\(vols[row])"
            case 1:
                return units[row]
            default:
                return ""
            }//end component
        case syringePickerView:
            return syringes[row]
        default:
            return ""
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView{
        case volsPickerView:
            switch component{
            case 1:
                selectedData!.selectedVolUnit = units[row]
                switch row{
                case 0:
                    vols = volsUL
                    pickerView.reloadAllComponents()
                case 1:
                    vols = volsML
                    pickerView.reloadAllComponents()
                default:
                    break
                    
                }//end switch row
                pickerView.selectRow(0, inComponent: 0, animated: true)
                selectedData!.selectedVol = vols[0]
            case 0:
                selectedData!.selectedVol = vols[row]
            default:
                break
            }//end switch component
        case syringePickerView:
            selectedData!.selectedSyringe = syringes[row]
        default:
            break
            
        }//end of switch pickerView
    }
}

