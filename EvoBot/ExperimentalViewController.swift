//
//  ExperimentalViewController.swift
//  EvoBot
//
//  Created by user on 21/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//

import UIKit


class ExperimentalViewController: UIViewController,GestureViewDelegate
{
    @IBOutlet weak var cameraWebView: UIWebView!
    var selectedData:SelectedData?{didSet{
        print (selectedData)}}
    var allData = [SelectedData]()
    var vessels: [Vessel]? = requestVessel()
    enum  VesselType {
    case Petridish, Wellplate96, Wellplate12
    }
    var experimentStartVesselType: String?
    var experimentEndVesselType: String?
    var experimentStartVesselID:String?
    var experimentEndVesselID:String?
    var startVessel:Vessel?
    var endVessel:Vessel?
    var startPoint:String?
    var endPoint:String?
    struct StoryboardConstants{
        static let pwSegue = "petridishToWellplate"
        static let ppSegue = "petridishToPetridish"
        static let wwSeque = "wellplateToWellplate"
        static let wpSegue = "wellplateToPetridish"
        static let teSegue = "To Run Experiment"
        static let seSegue = "Show Saved Experiments"
    
    }
    @IBAction func unwindAddToExperiment(segue: UIStoryboardSegue){
        if let svc = segue.sourceViewController as? PetriWellplateViewController{
            selectedData = svc.selectedData
            selectedData?.startPoint = startPoint
            selectedData?.endPoint = endPoint
            allData.append(selectedData!)
        
        }
        
        if let svc = segue.sourceViewController as? PetriPetriViewController{
            selectedData = svc.selectedData
            allData.append(selectedData!)
            
        }

    print (allData)
    }
    
    @IBOutlet weak var gestureView: GestureView!{
        didSet{
            gestureView.delegate = self
            gestureView.addGestureRecognizer(UIPanGestureRecognizer(target: gestureView, action: "drawHand:"))
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier{
        switch (identifier){
        case StoryboardConstants.pwSegue:
            if let pwvc = segue.destinationViewController.contentViewController as? PetriWellplateViewController{
                if let ppc = pwvc.popoverPresentationController{
                    ppc.sourceRect = endVessel!.imageView.frame
                }//end if let
        print ("performed segue")
            }//end if let
        
        case StoryboardConstants.ppSegue:
            if let ppvc = segue.destinationViewController.contentViewController as? PetriPetriViewController{
                if let ppc = ppvc.popoverPresentationController{
                    ppc.sourceRect = endVessel!.imageView.frame
                }//end if let
                print ("performed segue")
            }//end if let
            
        case StoryboardConstants.teSegue:
            if let revc = segue.destinationViewController.contentViewController as? RunViewController{
            revc.allData = allData
            }
        default:
            break
    }//end of switch
        }
    }//end of prepare
    override func viewDidLoad() {
        let url = NSURL(string: "http://192.168.2.3:8080/webcam/stream.html")
        let requestObj = NSURLRequest(URL: url!)
        cameraWebView.loadRequest(requestObj)
        super.viewDidLoad()
        updateVessels()
        
        
    }
    func updateVessels(){
     
    for vessel in vessels!
        {
            
            
            //dish.imageView = UIImageView()
            //dish.imageView?.image = dish.image
            //dish.imageView?.sizeToFit()
            //view.addSubview(dish.imageView!)
            vessel.imageView.image = vessel.image
            vessel.imageView.sizeToFit()
            vessel.imageView.transform.tx = CGFloat(vessel.coordinate.y) * view.bounds.width
            vessel.imageView.transform.ty = CGFloat(vessel.coordinate.x) * view.bounds.height

            /*
            imageV?.transform.a = (view.bounds.height) * CGFloat(vessel.size.height) / (vessel.image!.size.height)
            imageV?.transform.d = (view.bounds.width) * CGFloat(vessel.size.width) / (vessel.image!.size.width)
            */
            
            
            view.insertSubview(vessel.imageView, atIndex: 1)

        }// end of for loop

    }// end of func updateVessels
    
    func gestureDidStart(gesture: UIGestureRecognizer) {
        print ("started")
        startPoint = "\(gesture.locationInView(cameraWebView))"
        print (startPoint)
        startVessel = vesselFor(gesture)
        //print (startVessel!.id)
    }//end of func
    
    
    func gestureDidEnd(gesture: UIGestureRecognizer) {
        print ("Ended")
        endPoint = "\(gesture.locationInView(cameraWebView))"
        print (endPoint)
        endVessel = vesselFor(gesture)
        if let segueIdentifier = segueIdentifierFor(startVessel, endVessel: endVessel){
        performSegueWithIdentifier(segueIdentifier, sender: nil)
        }
        
        
        
    }//end of func gestureDidEnd
    
    
    func vesselFor(gesture:UIGestureRecognizer)->Vessel?{
        for vessel in vessels!{
            let containsPoint = vessel.imageView.frame.contains(gesture.locationInView(view))
            if containsPoint == true{
                return vessel
            }
            }
            return nil
            }//end of func
    func segueIdentifierFor(startVessel:Vessel?,endVessel:Vessel?)->String?{
        if let stVessel = startVessel, edVessel = endVessel{
        switch(stVessel.type){
            case "petridish":
                if edVessel.type == "wellplate" {return StoryboardConstants.pwSegue}
                if edVessel.type == "petridish"{return StoryboardConstants.ppSegue}
            case "wellplate":
                if edVessel.type == "wellplate" {return StoryboardConstants.wwSeque}
                if edVessel.type == "petridish"{return StoryboardConstants.wpSegue}
        default:
            break
        
        
        }//end switch
        }//end of if
        return nil
    
    
    }//end of func segueIdnetifier

}//end of viewcontroller



extension Vessel
{
    var image:UIImage?{
        get{
            if let image = UIImage(named: type){
        return image
            }
            else{
                return UIImage(named: "unknown_container")
            }
    }
    }
}

// MARK: - Convenience Extensions

extension UIViewController {
    var contentViewController: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController!
        } else {
            return self
        }
    }
}