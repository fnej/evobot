//
//  SelectedData.swift
//  EvoBot
//
//  Created by user on 29/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//

import Foundation



class SelectedData: NSObject, NSCoding {
    // MARK: Properties
    
    var selectedVol:Int?
    var selectedVolUnit:String?
    var selectedSyringe: String?
    var startPoint: String?
    var endPoint: String?
    
   
    
    // MARK: Archiving Paths
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("alldata")
    
    // MARK: Types
    
    struct PropertyKey {
        static let selectedVolKey = "selectedVol"
        static let selectedVolUnitKey = "selectedVolUnit"
        static let selectedSyringeKey = "selectedSyringe"
        static let startPoint = "startPoint"
        static let endPoint = "endPoint"
    }
    
    // MARK: Initialization
    
    init?(selectedVol: Int?, selectedVolUnit: String?, selectedSyringe: String?, startPoint: String?, endPoint: String?) {
        // Initialize stored properties.
        self.selectedVol = selectedVol
        self.selectedVolUnit = selectedVolUnit
        self.selectedSyringe = selectedSyringe
        self.startPoint = startPoint
        self.endPoint = endPoint
        
        super.init()
        
        // Initialization should fail if there is no name or if the rating is negative.
       
    }
    
    // MARK: NSCoding
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(selectedVol, forKey: PropertyKey.selectedVolKey)
        aCoder.encodeObject(selectedVolUnit, forKey: PropertyKey.selectedVolUnitKey)
        aCoder.encodeObject(selectedSyringe, forKey: PropertyKey.selectedSyringeKey)
        aCoder.encodeObject(startPoint, forKey: PropertyKey.startPoint)
        aCoder.encodeObject(endPoint, forKey: PropertyKey.endPoint)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let selectedVol = aDecoder.decodeObjectForKey(PropertyKey.selectedVolKey) as! Int?
        
        // Because photo is an optional property of Meal, use conditional cast.
        let selectedVolUnit = aDecoder.decodeObjectForKey(PropertyKey.selectedVolUnitKey) as! String?
        
        
        let selectedSyringe = aDecoder.decodeObjectForKey(PropertyKey.selectedSyringeKey) as! String?
        let startPoint = aDecoder.decodeObjectForKey(PropertyKey.startPoint) as! String?
        let endPoint = aDecoder.decodeObjectForKey(PropertyKey.endPoint) as! String?
        
        // Must call designated initializer.
        self.init(selectedVol: selectedVol, selectedVolUnit: selectedVolUnit, selectedSyringe: selectedSyringe, startPoint: startPoint, endPoint: endPoint)
    }
    
}