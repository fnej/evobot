//
//  Syringe.swift
//  stack
//
//  Created by user on 26/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//
import UIKit
import Foundation

struct Syringe{
 
    let id:String?
    let button = UIButton()
    
    
}



func availableSyringes() -> [String]
{
    let syringes: [String] = ["16", "10", "15", "1", "2"]
    
    return syringes
    
}// end of func request

func firstRowM() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "16"),
        Syringe(id: "14"),
        Syringe(id: "12")
    ]
    return syringes
}// end of func

func secondRowM() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "11"),
        Syringe(id: "9"),
        Syringe(id: "7")
    ]
    return syringes
}// end of func

func thirdRowM() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "6"),
        Syringe(id: "4"),
        Syringe(id: "2")
    ]
    return syringes
}// end of func

func forthRowM() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "1"),
        Syringe(id: "18"),
        Syringe(id: "0")
    ]
    return syringes
}// end of func

func firstRowL() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "15"),
        Syringe(id: "13"),
    ]
    return syringes
}// end of func

func secondRowL() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "10"),
        Syringe(id: "8"),
    ]
    return syringes
}// end of func

func thirdRowL() -> [Syringe]
{
    let syringes: [Syringe] = [
        Syringe(id: "5"),
        Syringe(id: "3"),
    ]
    return syringes
}// end of func