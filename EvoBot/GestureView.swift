//
//  GestureView.swift
//  EvoBot
//
//  Created by user on 22/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//

import UIKit

protocol GestureViewDelegate:class{
    func gestureDidStart(gesture: UIGestureRecognizer)
    func gestureDidEnd(gesture:UIGestureRecognizer)
}

class GestureView: UIView {
    
    var delegate:GestureViewDelegate?
    var lineWidth: CGFloat = 3 { didSet { setNeedsDisplay() } }
    var color: UIColor = UIColor.blueColor() { didSet { setNeedsDisplay() } }
    var scale: CGFloat = 0.90 { didSet { setNeedsDisplay() } }
    var handLocation:CGPoint = CGPoint(x: 10, y: 10)
    var location = CGPointZero
    var handLocations:[CGPoint] = [] {didSet{setNeedsDisplay()
        }
    }
    
        func drawHand(gesture: UIPanGestureRecognizer) {
        switch gesture.state{
        case .Began:
        handLocations = []
        print ( "location: \(gesture.locationInView(self))")
        location = gesture.locationInView(self)
        delegate?.gestureDidStart(gesture)
        
        
        case .Changed:
        
        print (gesture.translationInView(self))
        handLocation.x = location.x + gesture.translationInView(self).x
        handLocation.y = location.y + gesture.translationInView(self).y
        handLocations.append(handLocation)
        case .Ended:
            delegate?.gestureDidEnd(gesture)
        
        
        
        //gesture.setTranslation(CGPointZero, inView: faceView)
        default:
        break
        
        
        }
        
        
        
        }


    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let handDrawingPath = UIBezierPath()
        handDrawingPath.moveToPoint(location)
        for hndlc in handLocations{
            handDrawingPath.addLineToPoint(hndlc)
        }
        
        UIColor.grayColor().set()
        handDrawingPath.lineWidth = 3
        handDrawingPath.stroke()
    }
    

}
