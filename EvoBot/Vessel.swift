//
//  Vessel.swift
//  EvoBot
//
//  Created by user on 21/11/15.
//  Copyright © 2015 Feri Ami. All rights reserved.
//

import Foundation
import UIKit

struct Vessel
{
    let id: String
    let type:String
    let size:(height:Double, width:Double)
    let coordinate: (x:Double, y: Double)
    let imageView = UIImageView()
   
}
struct Constants{
    static let robotXLimits:Double = 185
    static let robotYLimits:Double = 225

}

    func requestVessel() -> [Vessel]
    {
        let vessels: [Vessel] = [
            Vessel(id: "0", type: "petridish", size: (height: 0.05, width: 0.05), coordinate: (x:80/Constants.robotXLimits,y:130/Constants.robotYLimits)),
            Vessel(id: "1", type: "petridish", size: (height: 0.05, width: 0.05), coordinate: (x:50/Constants.robotXLimits,y:50/Constants.robotYLimits)),
            Vessel(id: "2", type: "wellplate", size: (height: 0.1, width: 0.1), coordinate: (x:10/Constants.robotXLimits,y:120/Constants.robotYLimits)),
            Vessel(id: "3", type: "wellplate", size: (height: 0.1, width: 0.1), coordinate: (x:110/Constants.robotXLimits,y:10/Constants.robotYLimits)),
            /* Vessel(id: "3", type: "wellplate", size: (height: 0.1, width: 0.1), coordinate: (x:150/Constants.robotXLimits,y:180/Constants.robotYLimits)) */
        
        ]
        
        return vessels
    
    }// end of func requestvessel



